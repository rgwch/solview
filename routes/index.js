/*
 * Copyright (c) 2015 by G. Weirich
 */
var express = require('express');
var router = express.Router();

/* GET home page. This ist legacy code from Express.js. At this time, we just redirect to strings.js*/
router.get('/', function(req, res, next) {
  res.redirect(301, '/strings/1.11.2015');
});

module.exports = router;
