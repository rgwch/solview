/*
 * Copyright (c) 2015 by G. Weirich

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/*
 Some methods for PV-String comparison. Solar panels are organized in strings. Here we assume 2 strings, wich probably is a
 quite common setting with Fronius(tm) inverters.
 Panels within a string are dependent of each other. Thus, if one panel is defective or obstructed by shadow or dirt, the whole
 string's power production will degrade. This application helps to indentify such problems.

 Here we show string data individually
 */
var express = require('express');
var router = express.Router();
var moment = require('moment');
var sd = require('../models/pv_stringdata');
var caller = require('../models/httpclient')
var dateformats = ["DD.MM.YYYY", "YYYY-MM-DD", "YYYYMMDD"]
var _=require('underscore')

/**
 * request data of a given date. Format: /strings/setDate?date=DATE
 * where DATE is of one of the forms: DD.MM.YYYY, YYYY-MM-DD, or YYYYMMDD
 */
router.get("/setDate",function(req, res){
  var dat=moment(req.query.date,dateformats)
  if(dat.isValid()){
    sd.getStringData(dat.format("YYYY-MM-DD"),function(err,data){
      if(err){
        res.render('error',{message: "Fehler bei der Abfrage", error:err})
      }else{
        doRenderStrings(res,data, dat.format("DD.MM.YYYY"))
      }
    })
  }else{
    res.render('error',{message: "ungültiges Datum"})
  }

})

/**
 * Move the current date forward or backward (used by the respective buttons in strings.jade)
 */
router.get("/move", function(req,res){
  var dat=moment(req.query.date,dateformats)
  if(dat.isValid()) {
    dat.add(req.query.offset,'days')
    sd.getStringData(dat.format("YYYY-MM-DD"), function(err,data){
      if(err){
        res.render('error',{message: "Fehler bei der Abfrage", error:err})
      }else{
        doRenderStrings(res,data, dat.format("DD.MM.YYYY"))
      }
    })
  }
})

var doRenderStrings=function(res,data,datstr){
  if(data && data.String1Amperes) {
    var output = [
      {title: "<b>Parameter</b>", s1: "<b>String 1</b>", s2: "<b>String 2</b>"},
      getMaxAmp(data),
      getMaxVolts(data)
    ]
    addPowerInfo(output, data)
    res.render('strings', {datum: datstr, "data": output})
  }else{
    res.render('error',{message: "Keine Daten für dieses Datum vorhanden"})
  }
}

/**
 * Delete all data from the database to start over. Of course, this is disabled in production environment ;)

router.get("/initialize", function (req, res) {
  sd.initialize()
  res.render('strings', {datum: "initialized"})
})
*/

/**
 * Load data into the database. For this method to work,access to the Fronius(tm) datamanager is necessary
 */
router.get("/load/:from/:until", function (req, res) {
  var from = moment(req.params.from, dateformats)
  var until = moment(req.params.until, dateformats)
  getStringResults(from,until)
  res.render('strings',{datum: "loaded data from "+from.format("DD.MM.YYYY")+ " until "+until.format("DD.MM.YYYY"), data: []} )
})

// the fronius datamanager gets confused if too many requests arrive too fast. So we chain them here.
var getStringResults = function (act,last) {
    var dat=act.format("DD.MM.YYYY")
    console.log("requesting: "+dat)
    caller.call('GetArchiveData.cgi?Scope=System&StartDate=' + dat + '&EndDate=' + dat +
      '&Channel=Current_DC_String_1&Channel=Current_DC_String_2&Channel=Voltage_DC_String_1&Channel=Voltage_DC_String_2&Channel=EnergyReal_WAC_Sum_Produced',
      function(result){
        console.log("received "+dat)
        console.log(JSON.stringify(result.Body))
        var raw = result.Body.Data
        if(raw["inverter/1"]) {
          raw = raw["inverter/1"]
          sd.storeStringData(raw.Start, raw.Data)
        }
        if(act.isBefore(last)){
          act.add(1,'days')
          getStringResults(act,last)
        }
      })
}

/**
 * Get data of a given date. This is an alternative version of setDate()
 * Query form: /strings/DATE
 */
router.get("/:date?", function (req, res) {
  var datstr = ""
  if (req.params.date !== undefined) {
    var m = moment(req.params.date, dateformats)
    datstr += m.format("DD.MM.YYYY")
    sd.getStringData(m.format("YYYY-MM-DD"), function (err, data) {
      if (err) {
        res.render('error', {message: "Fehler bei der Abfrage ", error: err})
      } else {
        doRenderStrings(res,data, datstr)
      }
    })
  } else {
    res.render('strings', {datum: datstr, data: []})
  }
})


/* Some helper functions */
var getMaxAmp=function(data){
  var max1=getMax(data,"String1Amperes")
  var max2=getMax(data,"String2Amperes")
  return {title: "Maximaler Strom (A)", s1: max1, s2:max2}
}

var getMaxVolts=function(data){
  var max1=getMax(data,"String1Volts")
  var max2=getMax(data,"String2Volts")
  return {title: "Maximale Spannung (V)", s1: max1, s2:max2}
}

var getWatts=function(data){
  var w1= _.map(data.String1Amperes,function(val,key){return val*data.String1Volts[key]})
  var w2= _.map(data.String2Amperes,function(val,key){return val*data.String2Volts[key]})
  return {keys: _.keys(data.String1Amperes), w1: w1,w2:w2}
}

var addPowerInfo=function(output,data){
  var watts=getWatts(data)
  output.push({title: "Maximale Leistung (W)", s1: Math.round(_.max(watts.w1)), s2: Math.round(_.max(watts.w2))})
  var counted=0
  var sum1= watts.w1.reduce(function(cumul,elem){if(elem>0){counted++} return cumul+elem})
  var sum2= watts.w2.reduce(function(cumul,elem) {return cumul+elem})
  output.push({title: "Duchschnittsleistung (W)",s1: Math.round(sum1/counted), s2: Math.round(sum2/counted)})

  var seconds=watts.keys[1]-watts.keys[0]
  var cumulated1=0
  var cumulated2=0
  var maxdiff=0
  var diff1=0
  var diff2=0
  for(var i=0;i<watts.w1.length;i++){
    cumulated1+=watts.w1[i]*seconds;
    cumulated2+=watts.w2[i]*seconds
    var diff=watts.w1[i]-watts.w2[i]
    if(Math.abs(diff)>Math.abs(maxdiff)){
      maxdiff=diff
      diff1=watts.w1[i]
      diff2=watts.w2[i]
    }
  }
  output.push({title: "Total erzeugte Energie (Wh)", s1: Math.round(cumulated1/3600), s2: Math.round(cumulated2/3600)})
  output.push({title: "Maximale Leistungsdifferenz (W)", s1: Math.round(diff1), s2: Math.round(diff2)})
}
var getMax=function(data, coll){
  var val= _.values(data[coll])
  var max= _.foldl(val,function(result,item){return Math.max(result,item)})
  return max
}

module.exports = router
