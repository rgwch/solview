/**
 * Created by gerry on 22.11.15.
 */

/**
 REST API for solview data
 */

var express = require('express');
var router = express.Router();
var moment = require('moment');
var sd = require('../models/pv_stringdata');
var caller = require('../models/httpclient')
var dateformats = ["DD.MM.YYYY", "YYYY-MM-DD", "YYYYMMDD"]

router.get("/get/:date?", function (req, res) {
  var datstr = ""
  if (req.params.date !== undefined) {
    var m = moment(req.params.date, dateformats)
    datstr += m.format("DD.MM.YYYY")
    sd.getStringData(m.format("YYYY-MM-DD"), function (err, data) {
      if (err) {
        res.status(500).json({status: "error",message: "error retrieving data: "+err.message})
      } else {
        res.status(200).json({status: "ok", result: data})
      }
    })
  } else {
    res.render('strings', {datum: datstr, data: []})
  }
})

router.get("/graph/:date", function(req,res){
  res.render('graph', {datum: req.params.date})
})

module.exports=router