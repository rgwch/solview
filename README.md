Read and analyze production values of solar power installations with Fronius(tm) inverters.

Data are read from the Fronius(tm) datamanager and stored in a mongo database for later processing.
The database can be inhouse or elsewhere to allow public access.

## Install and run

 * install node.js and git. Set up a mongo server.
 * git clone https://gitlab.com/rgwch/solview.git
 * cd solview
 * npm install
 * copy config.json.sample to config.json and edit for your environment
 * npm start
 
## Modify

The project contains IntelliJ project files. So you might use [Idea](https://www.jetbrains.com/idea/) 
or [WebStorm](https://www.jetbrains.com/webstorm/) to work on the project. But since it's just JavaScript, any other
editor/IDE will do as well.

## Where to start

Solview is a Nodejs/Express application. So you'd probably start there: [http://expressjs.com](http://expressjs.com)

## License

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
