/*
 * Copyright (c) 2015 by G. Weirich

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/*
graphical display of string values. Uses d3.js.
 */

var loadData = function (datum) {
  var request = new XMLHttpRequest()
  request.onload = function () {
    displayD3(JSON.parse(request.response))
  }
  request.open("GET", "/api/v1/get/" + datum)
  request.send()

}

var displayD3 = function (answer) {
  // var testdata = [4, 8, 15, 16, 23, 42];
  if (answer.status === "ok") {
    var result = answer.result
    //var testdata = reduceObject(result.RealEnergy)
    var displaydata=simplifyData(result)
    graphVertBarChart(displaydata)
  }

}

var graphVertBarChart=function(result){
  var margin = {top: 20, right:20, bottom:30, left: 40}
  var width=1000 - margin.left-margin.right
  var height=640-margin.top-margin.bottom
  var barWidth= width/result.values.length

  var y = d3.scale.linear()
    .range([height, 0])
    .domain([0,5000])

  var x= d3.time.scale()
    .range([0,width])
    .domain([result.values[0].time,result.values[result.values.length-1].time])


  var chart = d3.select(".chart")
    .attr("width", width+margin.left+margin.right)
    .attr("height", height+margin.top+margin.bottom)




  var bar = chart.selectAll("g")
    .data(result.values)
    .enter().append("g")
    .attr("transform", function(d, i) { return "translate(" + (parseInt(margin.left) + (i * barWidth)) + ","+margin.top+")"; });

  bar.append("rect")
    .attr("y", function(d) { return y(d.watts1); })
    .attr("height", function(d) { return height - y(d.watts1); })
    .attr("width", barWidth - 1)
    .attr("class","string1")

  bar.append("rect")
    .attr("y",function(d) {return y(d.watts2);})
    .attr("height", function(d){return height-y(d.watts2); })
    .attr("width", barWidth-1)
    .attr("class","string2")
    .attr("opacity", "0.5")

  /*
  bar.append("text")
    //.attr("transform","rotate(-90); translate("+ barWidth + ","+margin.top+")")
    .attr("transform","rotate(-90)")
    .attr("x", barWidth / 2)
    .attr("y", height-50)
    .attr("dy", ".75em")
    .text(function(d) { return d; });
*/

  var xapos=height+margin.bottom
  var xAxis=d3.svg.axis().scale(x).orient("bottom")
    .tickFormat(d3.time.format("%H:%M"))

  chart.append("g")
    .attr("class","x axis")
    .attr("transform","translate("+parseInt(margin.left)+","+xapos+")")
    .call(xAxis)

   var yAxis=d3.svg.axis().scale(y).orient("left")
   chart.append("g")
   .attr("class", "y axis")
     .attr("transform","translate("+margin.left+","+margin.top+")")
   .call(yAxis)

}


var divBarChart=function(testdata){
  var x = d3.scale.linear()
    .domain([0, d3.max(testdata)])
    .range([0, 1000])
  var chart = d3.select(".chart")
  var bar = chart.selectAll("div")
  var barUpdate = bar.data(testdata)
  var barEnter = barUpdate.enter().append("div")
  barEnter.style("width", function (d) {
      return x(d) +"px"
    })
    .text(function (d) {
      return d
    })

}
var reduceObject = function (compound) {
  var values = []
  var times= []
  for (var key in compound) {
    var value=parseFloat(compound[key])
    if(value>1.0) {
      values.push(Math.round(value))
      times.push(key)
    }
  }
  return {"times": times, "values": values}
}

var simplifyData=function(obj){
  var simplified=[]
  var maxValue=0
  for(var key in obj.String1Amperes){
    var amp1=parseFloat(obj.String1Amperes[key])
    if(amp1>0.0){
      var amp2=parseFloat(obj.String2Amperes[key])
      var volt1=parseFloat(obj.String1Volts[key])
      var volt2=parseFloat(obj.String2Volts[key])
      var watts1=Math.round(amp1*volt1)
      var watts2=Math.round(amp2*volt2)
      maxValue=Math.max(maxValue,watts1)
      maxValue=Math.max(maxValue,watts2)

      simplified.push({time: new Date(parseInt(key)*1000),watts1: watts1, watts2: watts2})
    }
  }
  return {values: simplified, maxValue: maxValue}
}