/*
 * Copyright (c) 2015 by G. Weirich
 */

var http=require('http')
var nconf=require('nconf')
var host=nconf.get("smartmeter")
var prefix="/solar_api/v1/"

/**
 * Call the datalogger
 * @param query query string to call
 * @param callback
 */
exports.call=function(query, callback){
  http.get({
    host: host,
    path: prefix+query
  }, function(resp){
    var body=''
    resp.on('data', function(d){
      body+=d
    })
    resp.on('end', function(){
      callback(JSON.parse(body))
    })
  })
}
