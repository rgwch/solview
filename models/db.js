/*
 * Copyright (c) 2015 by G. Weirich
 */

/**
 * Connect to the mongo database (via mongoose ODM)
 */
var mongoose=require('mongoose')
var nconf=require('nconf')

// var dbUri= 'mongodb://10.71.83.1/expt'
var dbUri=nconf.get('database')
mongoose.connect(dbUri)

var db=mongoose.connection;

db.on("error",console.error.bind(console,'connection error :'))
db.once('open', function(callback){
  console.log("once")
})
db.on("connected", function(){
  console.log("connected")
})
db.on("disconnected", function(){
  console.log("disconnected")
})

var gracefulShutdown = function(msg,callback){
  db.close(function(){
    console.log("Closed "+msg)
    callback()
  })
};

process.once('SIGUSR2', function () {
  gracefulShutdown('nodemon restart', function () {
    process.kill(process.pid, 'SIGUSR2');
  });
});

process.on('SIGINT', function () {
  gracefulShutdown('app termination', function () {
    process.exit(0);
  });
});

process.on('SIGTERM', function() {
  gracefulShutdown('Heroku app shutdown', function () {
    process.exit(0);
  });
});

require('./pv_stringdata')
