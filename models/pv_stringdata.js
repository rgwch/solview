/*
 * Copyright (c) 2015 by G. Weirich
 */

/**
 * Mongoose ODM specification for PV-string data.
 */
var mongoose=require('mongoose')
var moment=require('moment')

var stringdata=new mongoose.Schema({
  "Date": {type: String, required: true},
  "String1Amperes": {},
  "String2Amperes": {},
  "String1Volts": {},
  "String2Volts": {},
  "RealEnergy": {}
})

Stringdata = mongoose.model('stringdata',stringdata)

exports.initialize=function(){
  Stringdata.remove({});

  var heute=new Stringdata({"Date": "2000-01-01","String1Amperes":[5,6,7], "String2Amperes": [8,9,10],
    "String1Volts":[10,20,30],"String2Volts":[40,50,60],"RealEnergy": [15,26,37]})

  heute.save(function(err,heute){
    if(err) console.error(err)
  })

}

exports.getStringData=function(date,callback){
  Stringdata.findOne({"Date": date},callback)
}

/**
 * Store string data for a given day in the database. If data for that day exist already, they will be overwritten
 * @param date day to insert
 * @param raw the raw string data to insert.
 */
exports.storeStringData=function(date,raw){
  var sampledate=moment(date)
  datstring=sampledate.format("YYYY-MM-DD")
  console.log("writing "+datstring)
  var item=new Stringdata({
    Date: datstring,
    String1Amperes: raw.Current_DC_String_1.Values,
    String2Amperes: raw.Current_DC_String_2.Values,
    String1Volts: raw.Voltage_DC_String_1.Values,
    String2Volts:  raw.Voltage_DC_String_2.Values,
    RealEnergy: raw.EnergyReal_WAC_Sum_Produced.Values
  })
  Stringdata.remove({Date: datstring}, function(err){
    if(err){
      console.log(err)
    }
  })
  item.save(function(err){
    if(err){
      console.log(datstring)
      console.error(err)
    }
  })
}