/**
 * Created by gerry on 31.10.15.
 */

define(['durandal/app', 'knockout', 'd3', 'durandal/system'], function (app, ko, d3, system) {

	drawChart= function () {
		var data = [4, 8, 15, 16, 23, 42,90]
		var width = 420, barHeight = 20;

		var x = d3.scale.linear()
			.domain([0, d3.max(data)])
			.range([0, width]);

		var chart = d3.select(".d3chart")
			.attr("width", width)
			.attr("height", barHeight * data.length);

		var bar = chart.selectAll("g")
			.data(data)
			.enter().append("g")
			.attr("transform", function (d, i) {
				return "translate(0," + i * barHeight + ")";
			});

		bar.append("rect")
			.attr("width", x)
			.attr("height", barHeight - 1);

		bar.append("text")
			.attr("x", function (d) {
				return x(d) - 3;
			})
			.attr("y", barHeight / 2)
			.attr("dy", ".35em")
			.text(function (d) {
				return d;
			});

	}


	return {
		displayName: 'summary',

		activate: function () {

			ko.bindingHandlers.d3ctl = {
				init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
					system.log("ko.handler init")
					drawChart()
				}
			}
		},

	}
})