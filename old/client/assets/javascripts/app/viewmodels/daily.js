/**
 * Created by gerry on 05.11.15.
 */

define(['durandal/app', 'knockout', 'd3', 'durandal/system', 'vertxbus'], function (app, ko, system, bus) {

	var stringData = ko.observable()

	var reload = function (date) {
		bus.send("solview", {"cmd": "dailypower"}, function (result) {
			stringData = {
				"0": [10, 20, 30, 40, 50, 20, 30]
			}
		})
	}

	return {
		activate: function(){
			d3.select("d3fld")
				.append("svg")
				.attr("width",640)
				.attr("height",480)
			reload("22.10.2015")
			stringData.subscribe(function(newValue){

			})
		}
	}
})