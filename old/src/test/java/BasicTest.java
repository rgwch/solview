import ch.weirich.solview.Runner;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by gerry on 26.10.15.
 */
@RunWith(VertxUnitRunner.class)
public class BasicTest {
  static EventBus eb;

  @BeforeClass
  public static void setUp(){

    eb = new Runner().getVertx().eventBus();
  }

  @Test(timeout=5000)
  public void getVersion(TestContext ctx) {
    Async async=ctx.async();
    eb.send("solview", new JsonObject().put("cmd", "version"), result -> {
      assertTrue(result.succeeded());
      JsonObject answer = (JsonObject) result.result().body();
      assertEquals("ok",answer.getString("status"));
      assertEquals("1.0.0", answer.getString("message"));
      async.complete();
    });

  }

  @Test(timeout=5000)
  public void getStringCoherence(TestContext ctx){
    Async async=ctx.async();
    eb.send("solview",new JsonObject().put("cmd","stringmatch").put("date","28.10.2015"), result -> {
      assertTrue(result.succeeded());
      JsonObject ans=(JsonObject)result.result().body();
      assertEquals("ok",ans.getString("status"));
      async.complete();
    });

  }
}
