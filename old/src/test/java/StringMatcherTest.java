import ch.weirich.solview.Runner;
import ch.weirich.solview.StringMatcher;
import io.vertx.core.json.JsonObject;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by gerry on 27.10.15.
 */
public class StringMatcherTest {

  @Test
  public void testStringMatcher(){
    Runner runner=new Runner();
    JsonObject parm=new JsonObject().put("date","28.10.2015");
    StringMatcher sm=new StringMatcher();
    JsonObject ret=sm.exec(parm);
    assertEquals("ok",ret.getString("status"));
  }
}
