import ch.weirich.solview.Runner;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestSuite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by gerry on 27.10.15.
 */
public class SolviewTests {

  public static void main(String[] args){
    TestSuite suite=TestSuite.create("SolView Tests");
    suite.test("BasicTests", context -> {
      EventBus eb = new Runner().getVertx().eventBus();
      Async async=context.async();
      eb.send("solview", new JsonObject().put("cmd", "version"), result -> {
        assertTrue(result.succeeded());
        JsonObject answer = (JsonObject) result.result().body();
        assertEquals("1.0.0", answer.getString("message"));
        async.complete();
      });
    });
    suite.run().awaitSuccess(10000L);
  }
}
