package ch.weirich.solview;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

import java.util.logging.Logger;

/**
 * Created by gerry on 25.10.15.
 */
public class Runner {

  static final String VERSION="1.0.0";
  static Vertx vertx;
  static MongoClient mongo;
  static MongoDatabase db;
  static Config cfg = ConfigFactory.load();
  static Logger log = Logger.getGlobal();
  String serverID;
  String clientID;



  public static void main(String[] argv) {
    System.out.println("Start");
    new Runner();
    if(argv.length>0){
      vertx.eventBus().send("client.catchup",new JsonObject().put("date",argv[0]));
    }
  }

  public Vertx getVertx(){
    return vertx;
  }
  public Runner() {
    vertx = Vertx.vertx();

    mongo = new MongoClient(cfg.getString("mongoHost"));
    db = mongo.getDatabase(cfg.getString("mongoDB"));

    vertx.deployVerticle("ch.weirich.solview.Server", result -> {
      if (result.succeeded()) {
        serverID = result.result();
        log.info("server deployed with id "+serverID);
      } else {
        log.severe("deployment of server failed " + result.cause().getMessage());
      }
    });

    vertx.deployVerticle("ch.weirich.solview.Client", result -> {
      if (result.succeeded()) {
        clientID = result.result();
        log.info("client deployed with ID "+clientID);
      } else {
        log.severe("deployment of client failed " + result.cause().getMessage() +result.result());

      }
    });

    vertx.eventBus().consumer("solview", new BusStop());
    log.info("eventBus connected");

  }


}
