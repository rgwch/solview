package ch.weirich.solview;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

/**
 * Created by gerry on 26.10.15.
 */
public class BusStop implements Handler<Message<JsonObject>> {
  StringMatcher stringMatcher=new StringMatcher();
  PowerLister powerLister=new PowerLister();

  @Override
  public void handle(Message<JsonObject> msg) {
    switch (msg.body().getString("cmd")){
      case "version": msg.reply(new JsonObject().put("message",Runner.VERSION).put("status", "ok")); break;
      case "stringmatch": msg.reply(stringMatcher.exec(msg.body())); break;
      case "dailypower": msg.reply(powerLister.exec(msg.body())); break;
      default: msg.reply(new JsonObject().put("status","error").put("message","undefined operation"));
    }
  }
}
