package ch.weirich.solview

import ch.rgw.tools.TimeTool
import io.vertx.core.json.JsonObject
import org.bson.Document

/**
 * Created by gerry on 27.10.15.
 */
class StringMatcher{

    fun exec(parm: JsonObject): JsonObject {
        val mc = Runner.db.getCollection("archive")
        val d1 = TimeTool()
        val date = TimeTool(parm.getString("date")).toString(TimeTool.DATE_COMPACT)
        val docs = mc.find(Document().append("date", date).append("type", "DC_Power"));
        val doc = docs.first()
        if (doc == null) {
            return JsonObject().put("status", "error").put("message", "no data")
        }
        val iData = getInverterData(doc)
        val String1Amperes = (iData?.get("Current_DC_String_1") as Document?)?.get("Values") as Document?
        val String2Amperes = (iData?.get("Current_DC_String_2") as Document?)?.get("Values") as Document?
        val diffsAmpere = String1Amperes?.mapValues { entry -> entry.value.toString().toDouble() - String2Amperes?.get(entry.key).toString().toDouble() }
        val cumulatedDifferenceAmps = diffsAmpere?.values?.fold(0.0) { total, next -> total + next }
        val String1Volts = (iData?.get("Voltage_DC_String_1") as Document?)?.get("Values") as Document?
        val String2Volts = (iData?.get("Voltage_DC_String_2") as Document?)?.get("Values") as Document?
        val diffsVolts = String1Volts?.mapValues { entry -> entry.value.toString().toDouble() - String2Volts?.get(entry.key).toString().toDouble() }
        val cumulatedDifferenceVolts = diffsVolts?.values?.fold(0.0) { total, next -> total + next }
        val String1Watts=String1Volts?.mapValues { entry -> entry.value.toString().toDouble()*String1Amperes?.get(entry.key).toString().toDouble() }
        val String2Watts=String2Volts?.mapValues { entry -> entry.value.toString().toDouble()*String2Amperes?.get(entry.key).toString().toDouble() }

        val RealEnergy = (iData?.get("EnergyReal_WAC_Sum_Produced") as Document?)?.get("Values") as Document?
        val cumulatedEnergy=RealEnergy?.values?.fold(0.0){total, next -> total.toString().toDouble() + next.toString().toDouble()}

        return JsonObject().put("status", "ok")
                .put("dA", cumulatedDifferenceAmps)
                .put("dV", cumulatedDifferenceVolts)
                .put("wh", cumulatedEnergy)
                .put("String1",String1Watts)
                .put("String2",String2Watts)
    }

    fun getInverterData(doc: Document): Document? {
        val head = doc.get("Head")
        val body = doc.get("Body") as Document?
        val data = body?.get("Data") as Document?
        val inverter = data?.get("inverter/1") as Document?
        return inverter?.get("Data") as Document?
    }


}