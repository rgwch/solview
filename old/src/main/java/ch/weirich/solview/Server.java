package ch.weirich.solview;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;


/**
 * Created by gerry on 26.10.15.
 */
public class Server extends AbstractVerticle {
  @Override
  public void start() throws Exception {
    super.start();
    HttpServerOptions httpOptions = new HttpServerOptions().setCompressionSupported(true);
    HttpServer httpd = vertx.createHttpServer(httpOptions);
    Router router = Router.router(vertx);

    SockJSHandler sockJSHandler = SockJSHandler.create(vertx);
    PermittedOptions po = new PermittedOptions().setAddress("solview");
    BridgeOptions options = new BridgeOptions().addInboundPermitted(po);
    sockJSHandler.bridge(options);

    router.route("/eventbus/*").handler(sockJSHandler);
    httpd.requestHandler(router::accept).listen(8080);

  }
}
