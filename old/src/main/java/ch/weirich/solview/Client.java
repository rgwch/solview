package ch.weirich.solview;

import ch.rgw.tools.TimeTool;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonObject;
import org.bson.Document;

/**
 * Created by gerry on 26.10.15.
 *
 */
public class Client extends AbstractVerticle {
  static final String API = "/solar_api/v1/";
  HttpClient client;

  @Override
  public void start() {
    HttpClientOptions options = new HttpClientOptions().setDefaultHost(Runner.cfg.getString("datalogger"));
    Runner.vertx.eventBus().consumer("client.catchup", msg ->{
      JsonObject body= (JsonObject) msg.body();
      TimeTool tt=new TimeTool(body.getString("date"));
      TimeTool today=new TimeTool();
      today.chop(2);
      while(tt.isBefore(today)){
        loadStringDCPower(tt.toString(TimeTool.DATE_ISO));
        tt.addDays(1);
      }
    });
    client = Runner.vertx.createHttpClient(options);
    Runner.vertx.setPeriodic(3600000L, time -> {
     // Runner.vertx.setTimer(100L,time ->{
      TimeTool tt = new TimeTool();
      Runner.log.info("loading data for "+tt.toString(TimeTool.DATE_GER));
      loadStringDCPower(tt.toString(TimeTool.DATE_ISO));
      loadInverterRealtimeData();
    });
  }

  void loadStringDCPower(String date) {
    StringBuilder sb = new StringBuilder(API);
    sb.append("GetArchiveData.cgi?Scope=System&StartDate=").append(date)
        .append("&EndDate=").append(date)
        .append("&Channel=Current_DC_String_1&Channel=Current_DC_String_2&Channel=Voltage_DC_String_1")
        .append("&Channel=Voltage_DC_String_2&Channel=EnergyReal_WAC_Sum_Produced&Channel=Temperature_Powerstage");

    loadAndStore(sb.toString(), "archive", "DC_Power", date);
    Runner.log.info("loadStringDCPower");
  }

  void loadStringDCVolt(String date){

  }

  void  loadMeterData(String date){
    StringBuilder sb=new StringBuilder(API);
    sb.append("GetMeterRealtimeData.cgi?Scope=System");
    loadAndStore(sb.toString(),"powerdata","RealWAC",date);
    Runner.log.info("loadMeterData");
  }

  void loadInverterRealtimeData() {
    StringBuilder sb = new StringBuilder(API).append("GetInverterRealtimeData.cgi?Scope=System");
    loadAndStore(sb.toString(), "realtime", "inverterSummary", new TimeTool().toString(TimeTool.DATE_ISO));
    sb = new StringBuilder(API).append("GetInverterRealtimeData.cgi?Scope=Device&DeviceId=1&DataCollection=MinMaxInverterData");
    loadAndStore(sb.toString(), "realtime", "inverterExt", new TimeTool().toString(TimeTool.DATE_ISO));
    Runner.log.info("loadInverterRealtimeData ");
  }

  void loadAndStore(String request, final String collection, final String datatype, final String querydate) {
    client.getNow(request, response -> {
      System.out.print(response.statusCode() + ", " + response.statusMessage());
      if (response.statusCode() == 200) {
        response.bodyHandler(buffer -> {
          try {
            String jss = buffer.toString();
            MongoCollection coll = Runner.db.getCollection(collection);
           // JsonObject jos=new JsonObject(jss);
            Document bss = Document.parse(jss);
            Document head = (Document) bss.get("Head");
            TimeTool ts = new TimeTool(querydate);
            bss.put("date", ts.toString(TimeTool.DATE_COMPACT));
            bss.put("type", datatype);
            FindOneAndReplaceOptions frop = new FindOneAndReplaceOptions().upsert(true);
            Object result=coll.findOneAndReplace(new Document().append("date", ts.toString(TimeTool.DATE_COMPACT)).append("type", datatype), bss, frop);
            if(result==null){
              Runner.log.info("inserted (Document not found) "+bss.toString());
            }else {
              Runner.log.info("updated " + datatype+" "+bss.toString());
            }
          }catch(Throwable ex){
            Runner.log.severe(ex.getMessage());
            ex.printStackTrace();
          }
        });
      } else {
        Runner.log.severe(response.statusCode() + ", " + response.statusMessage() + ": " + Runner.cfg.getString("datalogger") + request);
      }
    });
  }
}
